# -*- coding: utf-8 -*-

import argparse
import numpy as np
from pylab import *
import scipy
from scipy.misc import imread, imsave
from scipy.ndimage.morphology import grey_erosion, grey_dilation
from scipy.ndimage import filters as fil

#Scikit
from skimage import data
from skimage.filters import threshold_otsu
from skimage.filters.rank import median
from skimage.segmentation import clear_border
from skimage.measure import label
from skimage.morphology import closing, square, remove_small_objects, dilation, erosion, disk
from skimage.measure import regionprops
from skimage.color import label2rgb

structures = []

def get_window(radius):
	return [[1 for i in range(radius)] for j in range(radius)]

def get_bounds(image, l, h):
	pixels = []
	for i in range(l):
		for j in range(h):
			pixels.append(image[i][j])
	return max(pixels), min(pixels)

def thresholding(image, lim):
	#image = Image.open(input_file)
	l, h = image.shape

	min, max = get_bounds(image, l, h)
	for i in range(l):
		for j in range(h):
			#pnorm = (image[i][j]-min)/float((max-min))
			p = image[i][j]
			if p <= lim:
				image[i][j] = 0
			else:
				image[i][j] = 255
	return image

def remove_next_coin(image, str_el):
	radius = str_el
	
	while True:
		cc_before = count_connected_components(image)
		modified = erosion(image, square(radius))
		modified = dilation(modified, square(radius))
		cc_after = count_connected_components(modified)

		if cc_after != cc_before:
			modified = diff(image, modified)
			return modified, (cc_before-cc_after)

		radius += 1

def count_connected_components(image):
	l, h = image.shape

	labels = [[0 for i in range(h)] for j in range(l)]

	window = np.array([[1,1,1],
						[1,1,1],
						[1,1,1]], dtype=np.uint8)
	
	cur_label = 1
	queue = []
	visited = []
	for i in range(l):
		for j in range(h):
			#We want to mark all dark areas
			if image[i][j] == 255 and labels[i][j] == 0:
				queue.insert(0, (i,j))
				labels[i][j] = cur_label

				while len(queue) != 0:
					pos = queue.pop(0)
					#Analyze neighboors
					neighb = get_neighboors(image, pos[0], pos[1])
					for n in neighb:
						if image[n[0]][n[1]] == 255 and labels[n[0]][n[1]] == 0:
							labels[n[0]][n[1]] = cur_label
							queue.append((n[0],n[1]))
				cur_label += 1
	return cur_label-1

"""
Returns the difference between two images
"""
def diff(original, modified):
	l, h = original.shape

	queue = []
	labels = [[0 for i in range(h)] for j in range(l)]

	out = np.zeros((l,h))
	for i in range(l):
		for j in range(h):
			#Didn't draw this one yet
			if modified[i][j] == 255 and labels[i][j] == 0:
				queue.insert(0, (i,j))
				labels[i][j] = 1

				while len(queue) != 0:
					pos = queue.pop(0)
					neighb = get_neighboors(original, pos[0], pos[1])
					for n in neighb:
						if original[n[0]][n[1]] == 255 and labels[n[0]][n[1]] == 0:
							out[n[0]][n[1]] = 255
							labels[n[0]][n[1]] = 1
							queue.append((n[0],n[1]))
	return out
				
def get_neighboors(image, i, j):
	l, h = image.shape
	neighb = [((i+x)%l,(j+y)%h) for x in range(-1,2) 
								for y in range(-1,2)
								if i+x >= 0 and j+y >= 0 and
									i+x < l and j+y < h]
	return neighb

"""
Returns if the image is blank (all black)
"""
def is_blank(image):
	l, h = image.shape
	for i in range(l):
		for j in range(h):
			if image[i][j] == 255:
				return False
	return True

#Calibrating
def calibrate():
	coin_structs = {}

	for fname in os.listdir('./calibr/'):
		coin = fname.split('.')[0]
		coin_image = imread('./calibr/'+fname, flatten=True, mode='I')
		str_el, new_image = remove_next_coin(coin_image)

		coin_structs[coin] = str_el

def main():
	image = imread(input_file, flatten=True, mode='I').astype(np.uint8)
	image = median(image, disk(2))
	image = thresholding(image, lim).astype(np.uint8)
	imsave(input_file.split('.')[0]+'_lim.jpg', image)

	coin_structs = {}
	coin_structs[0.1] = 46
	coin_structs[0.05] = 50
	coin_structs[0.5] = 52
	coin_structs[0.25] = 57
	coin_structs[1] = 62

	coins = [0.1, 0.05, 0.5, 0.25, 1]

	for coin in coins:
		image, nr_removed = remove_next_coin(image, coin_structs[coin])
		if nr_removed > 0:
			print('Removed %d coins of %.2lf' % (nr_removed, coin))
			output_file = input_file.split('.')[0]+'_without_'+str(coin)+'.jpg'
			imsave(output_file, image)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='PIM')
	parser.add_argument('-i', dest='input', action='store', nargs=1, help='Input file')
	parser.add_argument('-lim', dest='lim', action='store', nargs=1, type=float, help='Thresholding k: [0,255]')

	args = parser.parse_args()

	input_file = args.input[0]
	lim = args.lim[0]

	main()
